import * as assert from "assert";
import * as stringify from "json-stringify-safe";


import Container from "@yinz/container/ts/Container";

const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');
const Asserts = container.getClazz('Asserts');

describe('| commons.Asserts', function () {

    /**
     *  Test Cases :
     *
     *  1 - Array contains subject >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an Exception
     *  2 - Array does not contain subject (reason and extra are supplied) >>>>>>>>>>> Should throw an Exception
     *  3 - Array does not contain subject (reason and extra are not supplied) >>>>>>> Should throw an Exception
     */
    describe('| #contains', function () {

        let array = [1, 2, 3];
        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Array contains subject >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an Exception', () => {

            try {
                // 1. prepare test data
                let subject = 1;

                // 2. execute test
                Asserts.contains(array, subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });


        it(`| Array does not contain subject (reason and extra are supplied) >>>>>>>>>>> Should throw an Exception`, () => {

            // 1. prepare test data
            let subject = 4;
            try {

                // 2. execute test
                Asserts.contains(array, subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.deepEqual(e.extra.array, array);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })

        it(`| Array does not contain subject (reason and extra are not supplied) >>>>>>> Should throw an Exception`, () => {

            // 1. prepare test data
            let subject = 4;
            try {

                // 2. execute test
                Asserts.contains(array, subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_CONTAINS__DOES_NOT_CONTAIN");
                assert.deepEqual(e.extra.array, array);
                assert.strictEqual(e.extra.subject, subject);
            }

        })

    });


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/

    /**
     *  Test Cases:
     *
     *  1 - Subject is array >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is not array (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  3 - Subject is not array (reason and extra are not supplied) >>>>>> Should throw an exception
     */
    describe('| # isArray', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is array >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = [1, 2, 3];

                // 2. execute test
                Asserts.isArray(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is not array (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 5;

            try {

                // 2. execute test
                Asserts.isArray(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not array (reason and extra are not supplied) >>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 4;
            try {

                // 2. execute test
                Asserts.isArray(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_ARRAY__IS_NOT_ARRAY");
                assert.strictEqual(e.extra.subject, subject);
            }

        })


    });


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is Date >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is not date (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  3 - Subject is not date (reason and extra are not supplied) >>>>>> Should throw an exception
     */
    describe('| # isDate', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is Date >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = new Date();

                // 2. execute test
                Asserts.isDate(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is not date (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 5;

            try {

                // 2. execute test
                Asserts.isDate(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not date (reason and extra are not supplied) >>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 4;
            try {

                // 2. execute test
                Asserts.isDate(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_DATE__IS_NOT_DATE");
                assert.strictEqual(e.extra.subject, subject);
            }

        })


    });


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is False >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is Undefined >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  3 - Subject is Null >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  4 - Subject is not falsy (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  5 - Subject is not falsy (reason and extra are not supplied) >>>>>> Should throw an exception
     */
    describe('| # isFalsy', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is False >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = false;

                // 2. execute test
                Asserts.isFalsy(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });


        it('| Subject is Undefined >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = undefined;

                // 2. execute test
                Asserts.isFalsy(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });


        it('| Subject is Null >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = null

                // 2. execute test
                Asserts.isFalsy(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });


        it(`| Subject is not falsy (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 5;

            try {

                // 2. execute test
                Asserts.isFalsy(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not falsy (reason and extra are not supplied) >>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 4;
            try {

                // 2. execute test
                Asserts.isFalsy(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_FALSY__IS_NOT_FALSY");
                assert.strictEqual(e.extra.subject, subject);
            }

        })


    });


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is Truthy >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is null >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception
     *  3 - Subject is false >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception
     *  4 - Subject is undefined >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception
     *  5 - Subject is not initialized >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception
     *  6 - Subject is not Truthy (reason and extra are not supplied) >>>>> Should throw an exception
     */
    describe('| # isTruthy', () => {

        let reason = 'ERR_ASSERTS_IS_TRUTHY__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is Truthy >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = true;

                // 2. execute test
                Asserts.isTruthy(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it('| Subject is null >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception', () => {

            let subject = null;
            try {

                // 2. execute test
                Asserts.isTruthy(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        });

        it('| Subject is false >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception', () => {

            let subject = false;
            try {

                // 2. execute test
                Asserts.isTruthy(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        });


        it('| Subject is undefined >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception', () => {

            let subject = undefined;
            try {

                // 2. execute test
                Asserts.isTruthy(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        });


        it('| Subject is not initialized >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw exception', () => {

            let subject;
            try {

                // 2. execute test
                Asserts.isTruthy(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual(e.extra.subject, subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        });


        it(`| Subject is not Truthy (reason and extra are not supplied) >>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = false;
            try {

                // 2. execute test
                Asserts.isTruthy(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_TRUTHY__IS_NOT_TRUTHY");
                assert.strictEqual(e.extra.subject, subject);
            }

        })

    });


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is Finite >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is not finite (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  3 - Subject is not finite (reason and extra are not supplied) >>>>>> Should throw an exception
     */
    describe('| # isFinite', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is Finite >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = 0;

                // 2. execute test
                Asserts.isFinite(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is not finite (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = NaN;

            try {

                // 2. execute test
                Asserts.isFinite(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not finite (reason and extra are not supplied) >>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = NaN;
            try {

                // 2. execute test
                Asserts.isFinite(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_FINITE__IS_NOT_FINITE");
                assert.strictEqual('' + e.extra.subject, '' + subject);
            }

        })

    })


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is is Non Empty String >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is empty string (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  3 - Subject is empty string (reason and extra are not supplied) >>>>>> Should throw an exception
     */
    describe('| # isNonEmptyString', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is is Non Empty String >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = 'swsw';

                // 2. execute test
                Asserts.isNonEmptyString(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is empty string (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = '';

            try {

                // 2. execute test
                Asserts.isNonEmptyString(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is empty string (reason and extra are not supplied) >>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = '';
            try {

                // 2. execute test
                Asserts.isNonEmptyString(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_NON_EMPTY_STRING__IS_EMPTY_STRING");
                assert.strictEqual('' + e.extra.subject, '' + subject);
            }

        })

    })


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is is Not Null >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is false (reason and extra are supplied) >>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  3 - Subject is undefined (reason and extra are supplied) >>>>>>>>>>>>>>>> Should not throw an exception
     *  4 - Subject is not initialized (reason and extra are supplied) >>>>>>>>>> Should not throw an exception
     *  5 - Subject is null (reason and extra are supplied) >>>>>>>>>>>>>>>>>>>>> Should throw an exception
     *  6 - Subject is null(reason and extra are not supplied) >>>>>>>>>>>>>>>>>> Should throw an exception
     */
    describe('| # isNotNull', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is is Not Null >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = 'swsw';

                // 2. execute test
                Asserts.isNotNull(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it('| Subject is false (reason and extra are supplied) >>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = false;

                // 2. execute test
                Asserts.isNotNull(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it('| Subject is undefined (reason and extra are supplied) >>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = undefined;

                // 2. execute test
                Asserts.isNotNull(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it('| Subject is not initialized (reason and extra are supplied) >>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject;

                // 2. execute test
                Asserts.isNotNull(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });


        it(`| Subject is null (reason and extra are supplied) >>>>>>>>>>>>>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = null;

            try {

                // 2. execute test
                Asserts.isNotNull(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        });


        it(`| Subject is null(reason and extra are not supplied) >>>>>>>>>>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = null;
            try {

                // 2. execute test
                Asserts.isNotNull(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_NOT_NULL__IS_NULL");
                assert.strictEqual('' + e.extra.subject, '' + subject);
            }

        })

    })


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is Number >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is a number string (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  3 - Subject is not a number (reason and extra are supplied) >>>>>>>>>>>>> Should throw an exception
     *  4 - Subject is not a number (reason and extra are not supplied) >>>>>>>>> Should throw an exception
     */
    describe('| # isNumber', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is Number >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = 15;

                // 2. execute test
                Asserts.isNumber(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is a number string (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = '15';

            try {

                // 2. execute test
                Asserts.isNumber(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })

        it(`| Subject is not a number (reason and extra are supplied) >>>>>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 'dasd';

            try {

                // 2. execute test
                Asserts.isNumber(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not a number (reason and extra are not supplied) >>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = 'dasd';
            try {

                // 2. execute test
                Asserts.isNumber(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_NUMBER__IS_NOT_NUMBER");
                assert.strictEqual('' + e.extra.subject, '' + subject);
            }

        })

    })


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is Object >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is not Object (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  3 - Subject is not Object (reason and extra are not supplied) >>>>>> Should throw an exception
     */
    describe('| # isObject', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is Object >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = {};

                // 2. execute test
                Asserts.isObject(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is not Object (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject;

            try {

                // 2. execute test
                Asserts.isObject(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not Object (reason and extra are not supplied) >>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = NaN;
            try {

                // 2. execute test
                Asserts.isObject(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_OBJECT__IS_NOT_OBJECT");
                assert.strictEqual('' + e.extra.subject, '' + subject);
            }

        })

    })



    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Member is a member of subject >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  1 - Subject is null and member is undefined >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw an exception
     *  2 - Subject is not Object (reason and extra are supplied) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw an exception
     *  3 - Member is not a string (reason and extra are not supplied) >>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw an exception
     */
    describe('| # isMember', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Member is a member of subject >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = { fld1: 'fld1-value' };

                // 2. execute test
                Asserts.isMember(subject, "fld1", reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is null and member is undefined >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = null;

            try {

                // 2. execute test
                Asserts.isMember(subject, undefined, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not Object (reason and extra are supplied) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = '';
            try {

                // 2. execute test
                Asserts.isMember(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_MEMBER__IS_NOT_OBJECT");
                assert.strictEqual('' + e.extra.subject, '' + subject);
            }

        })

        it(`| Member is not a string (reason and extra are not supplied) >>>>>>>>>>>>>>>>>>>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = { fld1: 'fld1-value' };

            try {

                // 2. execute test
                Asserts.isMember(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_MEMBER__MEMBER_NAME_IS_EMPTY_STRING");
                assert.strictEqual('' + e.extra.subject, '' + undefined); // subject is member the Error is thrown from non empty string
            }

        })

    })


    /****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************
     ****************************************************************************************************************************************************/


    /**
     *  Test Cases:
     *
     *  1 - Subject is String >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception
     *  2 - Subject is not String (reason and extra are supplied) >>>>>>>>>> Should throw an exception
     *  3 - Subject is not String (reason and extra are not supplied) >>>>>> Should throw an exception
     */
    describe('| # isString', () => {

        let reason = 'ERR_ASSERTS__UNKNOWN_DATA';
        let extra = { fld1: 'fld1-value' };


        it('| Subject is String >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Should not throw an exception', () => {

            try {
                // 1. prepare test data
                let subject = "s";

                // 2. execute test
                Asserts.isString(subject, reason, extra);

                // 3. assert result
            } catch (e) {
                if (e instanceof Exception) {
                    assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
                }
                else {
                    assert.ok(false, 'exception: ' + e.stack || e.message);
                }
            }

        });

        it(`| Subject is not String (reason and extra are supplied) >>>>>>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject;

            try {

                // 2. execute test
                Asserts.isString(subject, reason, extra);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, reason);
                assert.strictEqual('' + e.extra.subject, '' + subject);
                assert.strictEqual(e.extra.fld1, extra.fld1);
            }

        })


        it(`| Subject is not String (reason and extra are not supplied) >>>>>> Should throw an exception`, () => {

            // 1. prepare test data
            let subject = NaN;
            try {

                // 2. execute test
                Asserts.isString(subject);

                assert.ok(false, "bad flow: we should not be here as the test is expected to fire an exception!")

                // 3. assert result
            } catch (e) {

                // 3. assert result
                assert.ok(e instanceof Exception, e.stack || e.message);
                assert.strictEqual(e.reason, "ERR_ASSERTS_IS_STRING__IS_NOT_STRING");
                assert.strictEqual('' + e.extra.subject, '' + subject);
            }

        })

    })




});