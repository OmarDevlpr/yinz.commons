import * as assert from "assert";
import * as stringify from "json-stringify-safe";

import Container from "@yinz/container/ts/Container";

const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');

describe('| commons.Exception', function () {
    describe(' | #constructor', function () {
        it(' | should instantiate an object using new operator', function () {
            try {
                // 1. prepare test data;
                // 2. execute test
                const object = new Exception('ERR', { fld1: 'fld1-value' });


                // assert result
                assert.ok(object, stringify(object));
                assert.strictEqual(object.reason, 'ERR', stringify(object));
                assert.deepEqual(object.extra, { fld1: 'fld1-value' }, stringify(object));
                assert.strictEqual(object.message, 'ERR: ' + stringify({ fld1: 'fld1-value' }), stringify(object));
            } catch (e) {
                assert.ok(false, 'exception: ' + (e.stack || e.message));
            }
        });
    });
});
