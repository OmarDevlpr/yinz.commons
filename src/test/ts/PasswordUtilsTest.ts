import * as assert from "assert";


import Container from "@yinz/container/ts/Container";

const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        process.cwd() + '/dist/main/ts'
    ],
});


const PasswordUtils = container.getClazz('PasswordUtils');

describe('| commons.PasswordUtils', function () {
    describe(' | # hashPassword', function () {
        it(' | should produce the same result for the same input', async function () {
            try {

                // 1. prepare test data
                const password = "password";
                const salt = "salt";


                // 2. cxecute test
                const hash1 = await PasswordUtils.hashPassword(password, salt);
                const hash2 = await PasswordUtils.hashPassword(password, salt);


                // 3. assert result

                assert.strictEqual(hash1, hash2);

            } catch (e) {
                assert.ok(false, 'exception: ' + (e.stack || e.message));
            }
        });
    });
});
