import * as assert from "assert";
import * as rimraf from "rimraf";
import * as fs from "fs";



import Container from "@yinz/container/ts/Container";

const logFile = "/test.log";



describe('| commons.Logger', function () {
    describe('| log in console', function () {

        let tempDir = "./tmp";

        before((done) => {
            rimraf(tempDir, fs, () => {
                done();
            });
        });

        beforeEach((done) => {
            if (!fs.existsSync(tempDir)) {
                fs.mkdirSync(tempDir);
            }
            done();
        });

        afterEach((done) => {
            rimraf(tempDir, fs, () => {
                done();
            });
        });

        it('| should send logs to the console only', function () {

            const container = new Container({
                // paramsFile: 'params.yml',
                modules: [
                    process.cwd() + '/dist/main/ts'                    
                ],

                params: {
                    logger: {
                        // debug: true,
                        console: {},
                        // file: {root: '.'}
                    }
                }

            });
            
            const logger = container.getBean('logger');

            logger.error('Hello, Logger!');
            logger.warn('Hello, Logger!');
            logger.info('Hello, Logger!');
            logger.debug('Hello, Logger!');
            logger.trace('Hello, Logger!');

            assert.strictEqual(false, fs.existsSync(tempDir + logFile));

        });

        it('| should send logs to the console and file', function () {

            const container = new Container({
                // paramsFile: 'params.yml',
                modules: [
                    process.cwd() + '/dist/main/ts'
                ],

                params: {
                    logger: {
                        // debug: true,
                        console: {},
                        file: {
                            logPathFormat: tempDir + logFile,
                            splitFormat: "yyyymmdd",
                            level: "trace"
                        }
                    }
                }

            });
            
            const logger = container.getBean('logger');

            logger.error('Hello, Logger!');
            logger.warn('Hello, Logger!');
            logger.info('Hello, Logger!');
            logger.debug('Hello, Logger!');
            logger.trace('Hello, Logger!');



            assert.strictEqual(true, fs.existsSync(tempDir + logFile));


        });

    });
});
