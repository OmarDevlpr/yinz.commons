import Logger from "./Logger";

export interface YinzOptions {

    logger?: Logger;
    user?: string;
    __$$actionIsGranted$$__?: boolean;
    include?: string[];
    locale?: string;    
    context?: any;
}