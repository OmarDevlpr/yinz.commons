import * as stringify from "json-stringify-safe";

export default class Exception {

    public name: string;
    public message: string;
    public reason: string;
    public extra: any;

    constructor(reason: string, extra: any) {        
        // populate error details
        this.name = "Exception";
        this.message = extra && extra.message || (reason + ": " + stringify(extra));
        this.reason = reason;
        this.extra = extra;

        // include stack trace in error object
        Error.captureStackTrace(this, this.constructor);
    }


    toString() {
        return this.name + ": " + stringify(this.message) + `\n reason: ${this.reason} \n ${this.extra}`;
    }
}

