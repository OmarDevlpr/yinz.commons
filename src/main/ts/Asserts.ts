import * as _ from "lodash";
import Exception from "./Exception";
import * as deepExtend from "deep-extend";

export default class Asserts {


    /**
     *
     * @param {any[]} array
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static contains(array: any[], subject: any, reason?: string, extra?: any): void {
        if (array.indexOf(subject) === -1) {
            throw new Exception(reason || "ERR_ASSERTS_CONTAINS__DOES_NOT_CONTAIN", deepExtend(extra || {}, {
                array,
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isArray(subject: any, reason?: string, extra?: any): void {
        if (!_.isArray(subject)) {
            throw new Exception(reason || "ERR_ASSERTS_IS_ARRAY__IS_NOT_ARRAY", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isDate(subject: any, reason: string, extra: any): void {
        if (!_.isDate(subject)) {
            throw new Exception(reason || "ERR_ASSERTS_IS_DATE__IS_NOT_DATE", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isFalsy(subject: any, reason: string, extra: any): void {
        if (subject) {
            throw new Exception(reason || "ERR_ASSERTS_IS_FALSY__IS_NOT_FALSY", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isTruthy(subject: any, reason: string, extra: any): void {
        if (!subject) {
            throw new Exception(reason || "ERR_ASSERTS_IS_TRUTHY__IS_NOT_TRUTHY", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isFinite(subject: any, reason: string, extra: any): void {
        if (!_.isFinite(subject)) {
            throw new Exception(reason || "ERR_ASSERTS_IS_FINITE__IS_NOT_FINITE", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isNonEmptyString(subject: any, reason: string, extra: any): void {
        Asserts.isString(subject, reason, extra);
        if (_.isEmpty(subject)) {
            throw new Exception(reason || "ERR_ASSERTS_IS_NON_EMPTY_STRING__IS_EMPTY_STRING", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isNotNull(subject: any, reason: string, extra: any): void {
        if (_.isNull(subject)) {
            throw new Exception(reason || "ERR_ASSERTS_IS_NOT_NULL__IS_NULL", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isNumber(subject: any, reason: string, extra: any): void {
        if (!_.isNumber(subject)) {
            throw new Exception(reason || "ERR_ASSERTS_IS_NUMBER__IS_NOT_NUMBER", deepExtend(extra || {}, {
                subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isObject(subject: any, reason: string, extra: any): void {
        if (!_.isObject(subject)) {
            throw new Exception(reason || 'ERR_ASSERTS_IS_OBJECT__IS_NOT_OBJECT', deepExtend(extra || {}, {
                subject: subject
            }));
        }
    }

    /**
     *
     * @param subject
     * @param member
     * @param {string} reason
     * @param extra
     */
    public static isMember(subject: any, member: any, reason: string, extra: any): void {
        Asserts.isObject(subject, reason || 'ERR_ASSERTS_IS_MEMBER__IS_NOT_OBJECT', deepExtend(extra || {}, { subject: subject, member: member }));
        Asserts.isNonEmptyString(member, reason || 'ERR_ASSERTS_IS_MEMBER__MEMBER_NAME_IS_EMPTY_STRING', deepExtend(extra || {}, { subject: subject, member: member }));

        if (!subject.hasOwnProperty(member)) {
            throw new Exception(reason || 'ERR_ASSERTS_IS_MEMBER__IS_NOT_MEMBER', deepExtend(extra || {}, { subject: subject, member: member }));
        }
    }

    /**
     *
     * @param subject
     * @param {string} reason
     * @param extra
     */
    public static isString(subject: any, reason: string, extra: any): void {
        if (!_.isString(subject)) {
            throw new Exception(reason || 'ERR_ASSERTS_IS_STRING__IS_NOT_STRING', deepExtend(extra || {}, {
                subject: subject
            }));
        }
    }

}