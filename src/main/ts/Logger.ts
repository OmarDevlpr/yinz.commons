// IMPORTS
import * as tracer from "tracer";



// CONSTANTS
const PRIORITIES: any = {
    error: 0,
    warn: 10,
    info: 20,
    debug: 30,
    trace: 40,
};


// TYPES
type dest = "console" | "file";
type types = "error" | "warn" | "info" | "debug" | "trace";


/**
 *  Logger class
 */
export default class Logger {

    private options: any;
    private DEST: any;

    constructor(options: any) {

        this.options = options = options || {};


        if (options.debug) {
            console.log('logger options ' + JSON.stringify(options, null, 2));
        }


        this.DEST = {
            console: options.console ? (options.console.stackIndex = 2, tracer.colorConsole(options.console)) : null,
            file: options.file ? (options.file.stackIndex = 2, tracer.dailyfile(options.file)) : null,
        };

    }


    private log(dest: dest, type: types, message: string, ...extra: any[]): void {

        if (!this.DEST[dest] || PRIORITIES[type] > PRIORITIES[this.options[dest].level]) {
            return;
        }

        this.DEST[dest][type](message, ...extra);

    }


    error(message: string, ...extra: any[]): void {
        this.log('console', 'error', message, ...extra);
        this.log('file', 'error', message, ...extra);
    }

    warn(message: string, ...extra: any[]): void {
        this.log('console', 'warn', message, ...extra);
        this.log('file', 'warn', message, ...extra);
    }

    info(message: string, ...extra: any[]): void {
        this.log('console', 'info', message, ...extra);
        this.log('file', 'info', message, ...extra);
    }

    debug(message: string, ...extra: any[]): void {
        this.log('console', 'debug', message, ...extra);
        this.log('file', 'debug', message, ...extra);
    }

    trace(message: string, ...extra: any[]): void {
        this.log('console', 'trace', message, ...extra);
        this.log('file', 'trace', message, ...extra);
    }
}



