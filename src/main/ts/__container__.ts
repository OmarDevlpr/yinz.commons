
import Logger from "./Logger";
import Asserts from "./Asserts";
import Exception from "./Exception";
import Container from "@yinz/container/ts/Container";
import PasswordUtils from "./PasswordUtils";

const registerBeans = (container: Container) => {

    let logger = new Logger(container.getParam('logger'));

    container.setBean('logger', logger);    

};

const registerClazzes = (container: Container) => {

    container.setClazz('Logger', Logger);    
    container.setClazz('Asserts', Asserts);
    container.setClazz('Exception', Exception);    
    container.setClazz('PasswordUtils', PasswordUtils);    

};


export {
    registerBeans,
    registerClazzes
};