import * as crypto from "crypto"
import { promisify } from "util"

export default class PasswordUtils {

    /**
     * @method hashPassword
     * @param {string} password
     * @param {string} salt
     * @returns {Promise<string>}
     */
    public static async hashPassword(password: string, salt: string): Promise<string> {

        const pbkdf2Encrypt = promisify(crypto.pbkdf2);

        let hashedPassword = await pbkdf2Encrypt(password, salt, 4096, 30, 'sha512');

        return hashedPassword.toString('hex')


    }

}